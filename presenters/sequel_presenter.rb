# frozen_string_literal: true

class SequelPresenter < BasePresenter
  def as_json
    @object.values
  end
end
