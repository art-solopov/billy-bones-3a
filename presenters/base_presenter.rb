# frozen_string_literal: true

# TODO: rename into JsonPresenter
class BasePresenter < SimpleDelegator
  attr_reader :roda_app

  def initialize(roda_app, object)
    @roda_app = roda_app
    @object = object

    super(@object)
  end

  def as_json
    {}
  end

  def to_json(opts = nil)
    JSON.generate(as_json, opts)
  end
end
