# frozen_string_literal: true

module Expenses
  class IndexRowPresenter < BasePresenter
    STATUS_TAG_CLASSES = {
      _: %w[expense-tag],
      paid: %w[is-paid],
      urgent: %w[is-urgent],
      unpaid: %w[is-unpaid]
    }.freeze

    def status_tag
      key = case @object
            when :paid?.to_proc then :paid
            when :urgent?.to_proc then :urgent
            else :unpaid
            end
      text = I18n.t(key, scope: %i[expenses index tags])
      Tubby.new { |t| t.span(text, class: STATUS_TAG_CLASSES[:_] + STATUS_TAG_CLASSES[key]) }
           .to_html.html_safe
    end

    def paid_at
      p = super
      return '' unless p

      I18n.l(p, format: :short)
    end

    def payment_info
      return unless paid?

      "Paid at #{I18n.l(paid_at, format: :short)} via #{payment_method.name}"
    end
  end
end
