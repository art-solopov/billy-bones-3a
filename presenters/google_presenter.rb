# frozen_string_literal: true

class GooglePresenter < BasePresenter
  def client_id
    config.client_id
  end

  def login_uri
    "#{request.scheme}://#{request.host_with_port}/login"
  end

  private

  def request
    @object
  end

  def config
    @config ||= GoogleConfig.new
  end
end
