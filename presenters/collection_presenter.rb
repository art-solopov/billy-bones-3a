# frozen_string_literal: true

class CollectionPresenter < BasePresenter
  class << self
    attr_accessor :item_presenter
  end

  def as_json
    presented_collection.map(&:as_json)
  end

  alias objects presented_collection

  def presented_collection
    @presented_collection ||= @object.map { |e| self.class.item_presenter.new(roda_app, e) }
  end
end
