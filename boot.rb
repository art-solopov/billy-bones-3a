# frozen_string_literal: true

require 'bundler'

APP_ENV = ENV.fetch('APP_ENV', 'development')

Bundler.require(:default, APP_ENV)

AppLogger = Logger.new($stdout)

LOADER_IGNORED_DIRS = %w[
  public
  config
  routes
  helpers
  frontend
  node_modules
  views
  bin
  spec
].freeze

loader = Zeitwerk::Loader.new
zt_dirs = (Dir['*'].select { File.directory? _1 } - LOADER_IGNORED_DIRS)
zt_dirs.each { |dir| loader.push_dir(dir) }

loader.log! if ENV['ZEITWERK_LOG']

if APP_ENV == 'development'
  loader.enable_reloading
  zt_listener = Listen.to(*zt_dirs, only: /\.rb$/) do
    loader.reload
  end
  i18n_listener = Listen.to('config/locales', only: /\.yml$/) do
    I18n.reload!
  end

  zt_listener.start
  i18n_listener.start
end

loader.setup

require_relative './config/application'

Dir['./config/initializers/*.rb'].sort_by { |e| File.basename(e) }.each do |f|
  AppLogger.info "Initializer: #{f}"
  require_relative f
end

Unreloader = Rack::Unreloader.new(subclasses: %w[Roda], logger: AppLogger) { BillyBones }
