# frozen_string_literal: true

require_relative './boot'

# TODO: extract into separate files

namespace :db do
  desc 'Migrate database'
  task :migrate, [:version] do |_t, args|
    version = args[:version]&.to_i
    Sequel.extension :migration
    Sequel::Migrator.run(DB, 'db/migrate', target: version)
  end
end

namespace :generate do
  MIGRATION_TEMPLATE = <<~RB
    # frozen_string_literal: true

    Sequel.migration do
    end
  RB

  desc 'Generate migration'
  task :migration, [:message, :version] do |_t, args|
    args.with_defaults(version: Time.now.strftime('%Y%m%d%H%M%S'))
    migration_name = args.message.downcase.gsub(/\s+/, '_').gsub(/[^\w]+/, '')
    full_file_name = "db/migrate/#{args.version}_#{migration_name}.rb"
    puts "Generating #{full_file_name}"
    File.write(full_file_name, MIGRATION_TEMPLATE)
  end
end

namespace :data_migrations do # rubocop:disable Metrics/BlockLength
  desc 'Migrate tags to categories'
  task :tags_to_categories, [:top_category_tags] do |_t, args| # rubocop:disable Metrics/BlockLength
    top_category_tags = args.top_category_tags.split(';').map(&:downcase)
    top_categories = Category.for_names_with_parent(top_category_tags, nil).index_by { |ct| ct.name.downcase }
    (top_category_tags - top_categories.keys).each do |tag|
      top_categories[tag] = Category.create(name: tag, parent_id: nil)
    end
    problem_bills = []
    Expense.each do |bill|
      tags = bill.tags.to_a.map(&:downcase)
      next if tags.empty?

      top_category_tag = (tags & top_categories.keys).first || tags.first
      tags.delete(top_category_tag)
      if tags.size > 1
        problem_bills << [bill.id, 'More than 1 tag (except top category)']
        next
      end

      child_category_tag = tags.first
      category = top_categories[top_category_tag] ||
                 Category.for_name_with_parent(top_category_tag, nil) ||
                 Category.create(name: top_category_tag, parent_id: nil)
      if child_category_tag
        category = Category.for_name_with_parent(child_category_tag, category.id) ||
                   Category.create(name: child_category_tag, parent_id: category.id)
      end

      bill.category = category
      bill.save
    end

    problem_bills.each { |id, msg| puts "Problem id=#{id}: #{msg}" }
  end
end
