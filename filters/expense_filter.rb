# frozen_string_literal: true

class ExpenseFilter < Dry::Struct
  OptionalInteger = Types::Params::Integer | Types::Params::Nil

  transform_keys { |k| k.to_s.underscore.to_sym }

  attribute? :payment_method_id, OptionalInteger
  attribute? :state, Types::String.optional
  attribute? :period_year, OptionalInteger
  attribute? :category_id, OptionalInteger

  def call(base_scope)
    base_scope
      .then { |s| payment_method_id ? s.where(payment_method_id: payment_method_id) : s }
      .then { |s| state.present? ? s.where(state: state) : s }
      .then { |s| period_year ? s.where(Sequel.function(:date_part, 'year', Sequel[Expense.table_name][:period]) => period_year) : s }
      .then(&method(:by_category))
  end

  private

  def by_category(scope)
    return scope unless category_id

    scope.where(category: Category.where { |o| o.|(o.id =~ category_id, o.parent_id =~ category_id) })
  end
end
