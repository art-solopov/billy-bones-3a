# frozen_string_literal: true

module Google
  class Authenticate
    include Dry::Monads[:result]
    include Dry::Monads::Do.for(:call)

    def call(token)
      keys = fetch_keys
      payload = yield validate(token, keys)
      yield validate_email(payload['email'])

      Success(UserData.new(payload))
    end

    private

    def fetch_keys
      client.get.body
    end

    def config
      @config ||= GoogleConfig.new
    end

    def client
      @client ||= Faraday.new(config.certs_url) do |f|
        f.adapter :net_http
        f.response :json
      end
    end

    def validate(token, keys) # rubocop:disable Metrics/MethodLength
      payload, _header = JWT.decode(
        token, nil, true,
        algorithm: 'RS256',
        iss: config.issuer,
        verify_iss: true,
        verify_aud: true,
        jwks: keys,
        aud: config.client_id
      )
      Success(payload)
    rescue JWT::DecodeError => e
      Failure(e)
    end

    def validate_email(email)
      email.in?(config.permitted_emails) ? Success() : Failure("User not permitted")
    end
  end
end
