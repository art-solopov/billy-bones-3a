# frozen_string_literal: true

class ModelSave
  include FormProcessor
  include Dry::Monads::Do.for(:call)

  def initialize(contract_class, fields)
    @contract_class = contract_class
    @fields = fields
  end

  def call(model, params)
    contract = @contract_class.new
    form_data = yield validate_contract(contract, params)
    model = yield validate_model(model, form_data)
    Success(FormSuccess.new(form_data: form_data, model: model))
  end

  private

  def validate_model(model, data)
    model.set_fields(data, @fields)
    model.save
    Success(model)
  rescue Sequel::ValidationFailed
    Failure(FormFailure.new(form_data: data, errors: model.errors))
  end
end
