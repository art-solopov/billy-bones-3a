# frozen_string_literal: true

module Expenses
  class Pay
    include FormProcessor
    include Dry::Monads::Do.for(:call)

    def call(params)
      contract = PayExpensesContract.new
      form_data = yield validate_contract(contract, params)
      Expense.where(id: form_data[:bill_ids].split(/\s*,\s*/).map { Integer(_1) })
             .update(state: 'paid',
                     payment_method_id: form_data[:payment_method_id],
                     paid_at: form_data[:paid_at])
      Success()
    end
  end
end
