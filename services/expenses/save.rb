# frozen_string_literal: true

module Expenses
  class Save
    include FormProcessor
    include Dry::Monads::Do.for(:call)

    def call(bill, params)
      contract = ExpenseBasicInfoContract.new
      form_data = yield validate_contract(contract, params)
      model_data = build_model_data(form_data)
      model = yield validate_model(bill, model_data, form_data)
      Success(FormSuccess.new(form_data: form_data, model: model))
    end

    private

    def build_model_data(form_data)
      form_data.slice(:cost, :comment, :category_id).merge(
        period: Date.new(form_data[:period_year], form_data[:period_month], 1)
      )
    end

    def validate_model(model, data, form_data)
      model.set_fields(data, data.keys)
      model.save
      Success(model)
    rescue Sequel::ValidationFailed
      Failure(FormFailure.new(form_data: form_data, errors: model.errors))
    end
  end
end
