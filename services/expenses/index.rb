# frozen_string_literal: true

module Expenses
  class Index
    DEFAULT_PAGE_SIZE = 20
    Result = Struct.new(:scope, :filter_form, :collection, :pagination)

    def call(params)
      @params = params
      build_scope
      filter_scope
      paginate_scope

      Result.new(@scope, @filter, @scope.all, pagination)
    end

    private

    def build_scope
      @scope = Expense.dataset.order(
        Sequel.case({ { Sequel[Expense.table_name][:state] => 'init' } => -1 }, 0),
        Sequel.desc(:period),
        Sequel.asc(:id)
      ).eager(:payment_method, category: :parent)
    end

    def filter_scope
      @filter = ExpenseFilter.new(@params['q'])
      @scope = @filter.call(@scope)
    end

    def paginate_scope
      @scope = @scope.paginate(page, page_size)
    end

    def page
      Integer(@params['page'] || 1)
    end

    def page_size
      Integer(@params['page_size'] || DEFAULT_PAGE_SIZE)
    end

    def pagination
      {
        page: @scope.current_page,
        current_count: @scope.current_page_record_count,
        page_count: @scope.page_count,
        total_count: @scope.pagination_record_count
      }
    end
  end
end
