# frozen_string_literal: true

module FormProcessor
  include Dry::Monads[:result]

  class FormSuccess < Dry::Struct
    attribute :form_data, Types::Nominal::Any
    attribute :model, Types::Nominal::Any
  end

  class FormFailure < Dry::Struct
    attribute :form_data, Types::Nominal::Any
    attribute :errors, Types::Nominal::Hash
  end

  private

  def validate_contract(contract, params)
    result = contract.call(params)
    if result.success?
      Success(result.to_h)
    else
      Failure(FormFailure.new(form_data: result.to_h, errors: result.errors))
    end
  end
end
