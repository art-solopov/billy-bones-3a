# frozen_string_literal: true

module PaymentMethods
  class Save
    include FormProcessor
    include Dry::Monads::Do.for(:call)

    FIELDS = [:name].freeze

    def call(payment_method, params)
      contract = PaymentMethodContract.new
      form_data = yield validate_contract(contract, params)
      model = yield validate_model(payment_method, form_data)
      Success(FormSuccess.new(form_data: form_data, model: model))
    end

    private

    def validate_model(model, data)
      model.set_fields(data, FIELDS)

      model.save
      Success(model)
    rescue Sequel::ValidationFailed
      Failure(FormFailure.new(form_data: data, errors: model.errors))
    end
  end
end
