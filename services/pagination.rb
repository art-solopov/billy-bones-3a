# frozen_string_literal: true

class Pagination
  extend Forwardable

  def_delegators :@scope, :first_page?, :last_page?, :current_page, :page_range

  def initialize(roda_instance, request, scope, shared_attributes = {})
    @r = roda_instance
    @path = request.env['REQUEST_PATH']
    @params = request.params
    @scope = scope
    @shared_attributes = prepare_shared_attributes(shared_attributes)
  end

  def to_html
    tmpl = Tubby.new do |t|
      t.nav(class: :pagination, role: :pagination, 'aria-label': :pagination) do
        t << previous_page_link
        t << next_page_link

        t.menu(class: 'pagination-list') do
          page_range.each do |page|
            t.li page_link(page, class: ['pagination-link', ('is-current' if page == current_page)])
          end
        end
      end
    end

    tmpl.to_html
  end

  private

  def prepare_shared_attributes(attributes)
    attributes.reduce({}) do |s, (key, val)|
      if val.is_a?(Hash)
        s.merge(val.map { |subkey, subval| ["#{key}-#{subkey}".to_sym, subval.to_s] }.to_h)
      else
        s.merge(key.to_sym => val.to_s)
      end
    end
  end

  def previous_page_link
    return if first_page?

    page_link(current_page - 1, text: t(:prev), class: 'pagination-previous')
  end

  def next_page_link
    return if last_page?

    page_link(current_page + 1, text: t(:next), class: 'pagination-next')
  end

  def page_link(page, text: page.to_s, **kwargs)
    link_class = kwargs[:class]
    Tubby.new do |t|
      t.a(text, href: url_for_page(page), class: link_class, **@shared_attributes)
    end
  end

  def url_for_page(page)
    "#{@path}?#{Rack::Utils.build_nested_query(@params.merge('page' => page))}"
  end

  def t(key)
    I18n.t(key, scope: [:_pagination])
  end
end
