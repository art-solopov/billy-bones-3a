# frozen_string_literal: true

Unreloader.record_dependency('helpers', %w[billy_bones.rb])
Unreloader.require('helpers')

class BillyBones < Roda
  plugin :render, engine: 'slim', cache: APP_ENV != 'production',
                  layout_opts: { engine: 'erb' }
  plugin :partials
  plugin :public, headers: { 'cache-control' => 'no-cache' }
  plugin :hash_routes
  plugin :not_found
  plugin :sessions, secret: BillyBonesConfig.instance.session_secret
  plugin :cookies
  plugin :route_csrf
  plugin :halt
  plugin :all_verbs
  plugin :delegate
  plugin :forme_route_csrf
  plugin :request_headers
  plugin :flash

  request_delegate :halt, :redirect, :referrer, :session, :path

  include Dry::Monads[:result]

  include AuthHelper
  include FlashesHelper
  include I18nHelper
  include ButtonsHelper
  include CategoriesHelper
  include UnpolyHelper

  route do |r|
    r.public

    check_current_user! unless r.path.in? %w[/ /login]
    set_locale!

    response.headers['Vary'] = 'Accept, X-Up-Mode, X-Up-Target'

    r.root do
      view 'home/show'
    end

    r.is 'login' do
      @google = GooglePresenter.new(self, r)

      r.get do
        view 'login/show'
      end

      r.post do
        halt(403, 'Login unsuccessful') if request.params['g_csrf_token'] != r.cookies['g_csrf_token']

        response.delete_cookie('g_csrf_token')

        result = Google::Authenticate.new.call(request.params['credential'])
        case result
        in Success(UserData => user)
          self.current_user = user
          redirect '/'
        else
          view 'login/show'
        end
      end
    end

    r.hash_routes
  end

  not_found do
    '404: Page not found'
  end

  private

  def render_page(page)
    @page = page
    @title = @page.capitalize # TODO: replace with I18n?
    view 'page'
  end

  def title
    ['Billy Bones', @title].reject(&:blank?).join(' — ')
  end

  def validate_contract(contract_class, params = request.params)
    contract = contract_class.new
    result = contract.call(params)
    return halt(422, result.errors.to_h) if result.failure?

    result.to_h
  end

  def paginate(scope, **attrs)
    Pagination.new(self, request, scope, attrs).to_html
  end

  def url_for(base, params = {})
    "#{base}?#{Rack::Utils.build_nested_query(params)}"
  end

  def assets_host
    BillyBonesConfig.instance.assets_host
  end

  def process_result(result, success_redirect:, failure_render:, success_flash: nil)
    @result = result
    case @result
    when Success
      flash[:success] = success_flash if success_flash
      redirect success_redirect
    when Failure(FormProcessor::FormFailure)
      response.status = 422
      @form_data = result.failure.form_data
      @form_errors = result.failure.errors
      view failure_render
    else
      raise 'Unknown error'
    end
  end

  def success?
    @result.nil? || @result.success?
  end

  def failure?
    !success?
  end

  def set_locale!
    language = request.headers['Accept-Language']
                      .to_s
                      .split(/\s+/)
                      .map { |e| e[/^\w{2}/]&.downcase&.to_sym }
                      .compact
                      .find { |lg| I18n.available_locales.include?(lg) }
    I18n.locale = language if language
  end
end

Unreloader.record_split_class(__FILE__, 'routes')
Unreloader.require('routes') { [] }
