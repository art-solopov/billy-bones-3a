# frozen_string_literal: true

class PayExpensesContract < Dry::Validation::Contract
  params do
    required(:bill_ids).filled(:string, format?: /\d+(,\d+)*/)
    required(:paid_at).filled(:date)
    required(:payment_method_id).filled(:integer)
  end

  rule(:payment_method_id) do
    key.failure('payment method not found') unless PaymentMethod[Integer(value)]
  end
end
