# frozen_string_literal: true

class CategoryContract < Dry::Validation::Contract
  params do
    required(:name).filled(:string)
    required(:parent_id).maybe(:integer)
  end
end
