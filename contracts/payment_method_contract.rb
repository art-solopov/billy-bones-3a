# frozen_string_literal: true

class PaymentMethodContract < Dry::Validation::Contract
  params do
    required(:name).filled
  end
end
