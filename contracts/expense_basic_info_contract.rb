# frozen_string_literal: true

class ExpenseBasicInfoContract < Dry::Validation::Contract
  params do
    required(:period_year).filled(:integer, gt?: 2000)
    required(:period_month).filled(:integer, included_in?: (1..12))
    required(:cost).filled(:string, format?: /\d+(\.\d{0,2})?$/)
    required(:category_id).maybe(:integer)
    optional(:comment).maybe(:string)
  end
end
