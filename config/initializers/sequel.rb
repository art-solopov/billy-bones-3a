# frozen_string_literal: true

Sequel.extension :pg_array_ops
DB = Sequel.connect(BillyBonesConfig.instance.database)
DB.extension :pg_array, :pagination
DB.loggers << BillyBonesConfig.instance.logger
Sequel::Model.plugin :timestamps, update_on_create: true
Sequel::Model.plugin :validation_helpers
