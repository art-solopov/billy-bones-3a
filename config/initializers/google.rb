# frozen_string_literal: true

class GoogleConfig < Anyway::Config
  config_name :google

  attr_config(
    :client_id,
    permitted_emails: [],
    certs_url: 'https://www.googleapis.com/oauth2/v3/certs',
    issuer: ['accounts.google.com', 'https://accounts.google.com']
  )

  required :client_id

  coerce_types permitted_emails: { type: :string, array: true }
end
