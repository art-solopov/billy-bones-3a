# frozen_string_literal: true

Forme.register_transformer(:wrapper, :application) do |tag, input|
  if input.type == :hidden
    tag
  else
    input.tag(:div, { class: 'field' }, tag)
  end
end

# Forme.register_transformer(:error_handler, :bulma_error_handler) do |tag, input|
#   control_tag = Array(tag).find { |e| e.attr[:class].to_s.include?('control') }
#   input_tag = control_tag ? control_tag.children[0] : tag
#   Forme.attr_classes(input_tag.attr, 'is-danger')
#   Array(tag) + [input.tag(:p, { class: 'help is-danger' }, input.opts[:error])]
# end

class ApplicationLabeler < Forme::Labeler
  def call(tag, input)
    return super if %i[radio checkbox].include?(input.type)

    label = input.opts[:label]
    label_attrs = input.opts[:label_attr] || {}
    label_attrs[:for] = tag.attr[:id]
    Forme.attr_classes(label_attrs, 'label')

    [
      input.tag(:label, label_attrs, label),
      tag
    ]
  end
end

class ApplicationFormatter < Forme::Formatter
  private

  def convert_to_tag(type)
    input_tag = super

    if (prefix = form.opts[:prefix])
      name = input_tag.attr[:name]
      new_name = name.to_s.sub(/^[^\[]*/, '[\0]')
      input_tag.attr[:name] = "#{prefix}#{new_name}"
    end

    input_tag
  end
end

Forme.register_transformer(:formatter, :application, ApplicationFormatter)
Forme.register_transformer(:labeler, :application, ApplicationLabeler.new)

# Forme.register_config(:bulma,
#                       wrapper: :bulma_field,
#                       formatter: :bulma_formatter,
#                       labeler: :bulma_labeler,
#                       error_handler: :bulma_error_handler)
Forme.register_config(:application,
                      labeler: :application,
                      formatter: :application,
                      wrapper: :application)

Forme.default_config = :application
