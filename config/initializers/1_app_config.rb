# frozen_string_literal: true

class BillyBonesConfig < Anyway::Config
  include Singleton # TODO: maybe refactor?

  config_name :billy_bones

  attr_config(
    :timezone,
    logger: Logger.new($stdout),
    database: {},
    session_secret: '',
    enable_auth: true,
    assets_host: '',
    available_locales: [:en]
  )

  required :session_secret

  on_load :set_timezone

  private

  def set_timezone
    return if timezone.present?

    self.timezone = `timedatectl show --property=Timezone --value`.strip
  rescue Errno::ENOENT
    logger.warn 'No timedatectl present, setting timezone to UTC'
    self.timezone = 'UTC'
  end
end

Time.zone_default = Time.find_zone!(BillyBonesConfig.instance.timezone)
I18n.available_locales = BillyBonesConfig.instance.available_locales
