# frozen_string_literal: true

i18n_dir = File.expand_path(
  File.join(
    File.dirname(__FILE__),
    '../locales'
  )
)
I18n.load_path << Dir["#{i18n_dir}/**/*.yml"]
