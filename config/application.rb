# frozen_string_literal: true

Anyway::Settings.default_environmental_key = '_default'
Anyway::Settings.current_environment = APP_ENV
