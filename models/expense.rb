# frozen_string_literal: true

class Expense < Sequel::Model
  extend Enumerize

  many_to_one :payment_method
  many_to_one :category

  enumerize :state, in: %i[init paid], default: :init, i18n_scope: ['expense.state'],
                    predicates: true

  def urgent?
    !paid? && period < Time.zone.today.beginning_of_month
  end

  def form_data
    {
      period_month: period.month,
      period_year: period.year,
      cost: cost,
      payment_method_id: payment_method_id,
      comment: comment,
      category_id: category_id
    }
  end
end
