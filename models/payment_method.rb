# frozen_string_literal: true

class PaymentMethod < Sequel::Model
  one_to_many :expenses

  def validate
    super

    validates_unique :name
  end
end
