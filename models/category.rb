# frozen_string_literal: true

class Category < Sequel::Model
  many_to_one :parent, class: self
  one_to_many :children, key: :parent_id, class: self

  def self.roots
    where(parent_id: nil)
  end

  def self.roots_default_order_with_children
    roots.eager(children: proc { |ds| ds.order(:name) })
         .order(:name)
         .all
  end

  def self.for_names_with_parent(names, parent_id)
    where { |o| o.&(o.lower(o.name) =~ Array(names), o.parent_id =~ parent_id) }
  end

  def self.for_name_with_parent(name, parent_id)
    for_names_with_parent([name], parent_id).first
  end

  def validate
    super

    validates_presence :name
    validates_unique :name unless parent_id
    validates_unique %i[name parent_id]
    errors.add(:parent_id, 'cannot be a child of non-root category') if parent&.parent
  end
end
