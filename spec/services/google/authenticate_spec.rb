# frozen_string_literal: true

RSpec.describe Google::Authenticate do
  subject(:result) { service.call(token) }

  let(:service) { described_class.new }
  let(:config) do
    GoogleConfig.new(client_id: SecureRandom.hex(5), permitted_emails: permitted_emails)
  end
  let(:permitted_emails) { [user_email] }
  let(:client) { instance_double(Faraday::Connection) }
  let(:response_double) { instance_double(Faraday::Response) }
  let(:rsa_key) do
    OpenSSL::PKey::RSA.generate 2048
  end
  let(:kid) { SecureRandom.hex(3) }
  let(:jwk) { JWT::JWK.new(rsa_key, kid) }
  let(:user_name) { 'User User' }
  let(:user_email) { 'useruser@example.com' }
  let(:iss) { config.issuer.first }
  let(:aud) { config.client_id }
  let(:token) do
    payload = {
      iss: iss,
      aud: aud,
      name: user_name,
      email: user_email
    }
    headers = {
      kid: kid
    }

    JWT.encode payload, jwk.keypair, 'RS256', headers
  end

  before do
    allow(service).to receive(:config).and_return(config)
    allow(service).to receive(:client).and_return(client)
    allow(response_double).to receive(:body).and_return(
      { 'keys' => [jwk.export.transform_keys(&:to_s)] }
    )
  end

  describe 'success' do
    it 'decodes jwt' do
      expect(client).to receive(:get).and_return(response_double)
      expect(result).to be_success
      expect(result.value!.email).to eq(user_email)
      expect(result.value!.name).to eq(user_name)
    end
  end

  describe 'invalid claims' do
    context 'when aud is invalid' do
      let(:aud) { "#{config.client_id}1" }

      it 'verifies aud' do
        expect(client).to receive(:get).and_return(response_double)
        expect(result).to be_failure
        expect(result.failure).to be_a JWT::InvalidAudError
      end
    end

    context 'when iss is invalid' do
      let(:iss) { 'invalidissuer@example.com' }

      it 'verifies iss' do
        expect(client).to receive(:get).and_return(response_double)
        expect(result).to be_failure
        expect(result.failure).to be_a JWT::InvalidIssuerError
      end
    end
  end

  describe 'invalid email' do
    let(:permitted_emails) { ['nouser@example.com'] }

    it 'fails' do
      expect(client).to receive(:get).and_return(response_double)
      expect(result).to be_failure
      expect(result.failure).to eq('User not permitted')
    end
  end
end
