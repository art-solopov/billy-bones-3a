# frozen_string_literal: true

require 'rack/unreloader'

Unreloader = Rack::Unreloader.new { BillyBones }

module SpecHelpers
  module Routes
    include Rack::Test::Methods

    def app
      @app ||=
        begin
          Unreloader.require('./billy_bones.rb') { %w[BillyBones] }
          BillyBones.app
        end
    end
  end
end
