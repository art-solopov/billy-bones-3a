# frozen_string_literal: true

RSpec.describe 'root' do
  it 'fetches root' do
    get '/'
    expect(last_response).to be_ok
  end
end
