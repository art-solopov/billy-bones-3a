# frozen_string_literal: true

module I18nHelper
  private

  def model_name(model_class, plural: false)
    sub_scope = plural ? 'plural_name' : 'name'
    I18n.t(sub_scope, scope: model_scope(model_class))
  end

  def field_name(model_class, field)
    I18n.t(field, scope: model_scope(model_class) + [:fields])
  end

  # Path-specific translation
  def t(key = nil, *, scope: path_scope, **options)
    I18n.t(key, scope: scope, **options)
  end

  def model_scope(model_class)
    name = case model_class
           when String then model_class
           when ->(n) { n.respond_to?(:name) } then model_class.name
           else model_class.to_s
           end
    ['models', name.underscore]
  end

  def path_scope
    path.split('/').select(&:present?).reject { |e| e =~ /\A\d+\z/ }
  end

  def month_names
    Date::ABBR_MONTHNAMES.map { |e| I18n.t(e.downcase, scope: [:months]) if e }
  end
end
