# frozen_string_literal: true

module PathsHelper
  module_function

  def model_or_id(obj)
    case obj
    when Integer then obj
    when Hash then obj[:id] || obj['id']
    when ->(x) { x.respond_to?(:id) } then obj.id
    else raise ArgumentError, "Can't get id from #{obj}"
    end
  end
end
