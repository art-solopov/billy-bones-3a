# frozen_string_literal: true

module AuthHelper
  private

  def current_user
    @current_user ||= if BillyBonesConfig.instance.enable_auth
                        session['user_data'].then { |ud| UserData.new(ud) if ud }
                      else
                        UserData.new(email: '', name: '')
                      end
  end

  def current_user=(user_data)
    session['user_data'] = user_data.to_h
  end

  def check_current_user!
    return if current_user

    flash[:info] = 'Please log in'
    redirect '/login'
  end
end
