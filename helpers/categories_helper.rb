# frozen_string_literal: true

module CategoriesHelper
  CATEGORY_TITLE_SEPARATOR = ' / '

  private

  def category_options_for_expenses
    Category.roots_default_order_with_children.reduce([[]]) do |s, category|
      s + category.children.prepend(category).map { category_option _1 }
    end
  end

  def category_option(category)
    [category_title(category), category.id]
  end

  def category_title(category)
    [category.parent&.name, category.name].compact.join(CATEGORY_TITLE_SEPARATOR)
  end
end
