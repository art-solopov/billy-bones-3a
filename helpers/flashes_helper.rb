# frozen_string_literal: true

module FlashesHelper
  SHARED_FLASH_CLASSES = 'flash'

  FLASH_CLASSES = {
    success: 'success',
    error: 'danger',
    _: ''
  }.freeze

  private

  def flash_tags
    flash.map do |type, msg|
      tmpl = Tubby.new do |t|
        flash_classes = [SHARED_FLASH_CLASSES, FLASH_CLASSES.fetch(type.to_sym, FLASH_CLASSES[:_])]
        t.div(class: flash_classes, data: { controller: 'flash' }) do
          t.span(msg)
          t.button('🗙', class: 'close', data: { action: 'flash#close' })
        end
      end

      tmpl.to_html
    end
  end
end
