# frozen_string_literal: true

module UnpolyHelper
  MODAL_LINK_ATTRS = { 'up-follow': true, 'up-layer': 'new', 'up-history': 'false' }.freeze

  private

  def unpoly?
    request.headers['X-Up-Version'].present?
  end

  def unpoly_fragment?
    unpoly? && unpoly_target != 'body'
  end

  def unpoly_target
    request.headers['X-Up-Target']
  end

  def unpoly_mode
    header = success? ? 'X-Up-Mode' : 'X-Up-Fail-Mode'
    request.headers[header]
  end
end
