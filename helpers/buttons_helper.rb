# frozen_string_literal: true

module ButtonsHelper
  NEW_LINK_CLASSES = %w[button is-rounded is-primary].freeze

  private

  def new_link(text, path, **options)
    Tubby.new { |t| t.a(text, href: path, class: NEW_LINK_CLASSES, **options) }.to_html.html_safe
  end
end
