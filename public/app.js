import {Controller, Application} from '@hotwired/stimulus'

import {PayButtonController, PayFormController} from './bills_pay_controllers.js'

class FlashesController extends Controller {
    close() {
        up.destroy(this.element, {animation: 'fade-out'})
    }
}

function main() {
    const application = Application.start()
    application.register('flash', FlashesController)
    application.register('pay-button', PayButtonController)
    application.register('pay-form', PayFormController)
}

main()
