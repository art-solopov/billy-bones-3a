import {Controller} from '@hotwired/stimulus'

export class PayButtonController extends Controller {
    static targets = ['payButton', 'checkbox']

    get enabled() {
        return this.checkboxTargets.some(e => e.checked)
    }

    get billIds() {
        return this.checkboxTargets.filter(e => e.checked).map(e => e.value)
    }

    controlButton() {
        this.payButtonTarget.disabled = !this.enabled
    }

    connect() {
        this.controlButton()
    }
}

export class PayFormController extends Controller {
    static targets = ['billIdsInput']
    static outlets = ['pay-button']

    payButtonOutletConnected(outlet, _el) {
        this.billIdsInputTarget.value = outlet.billIds.join(',')
    }
}
