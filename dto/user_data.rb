# frozen_string_literal: true

class UserData < Dry::Struct
  transform_keys(&:to_sym)

  attribute :email, Types::String
  attribute :name, Types::String
  attribute? :picture, Types::String.optional
end
