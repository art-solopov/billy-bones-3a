# frozen_string_literal: true

require 'rack'
require_relative './boot'

Unreloader.require './billy_bones.rb'

use Rack::MethodOverride # TODO: move to Roda app
run Unreloader
