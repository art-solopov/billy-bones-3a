# frozen_string_literal: true

class BillyBones
  EXPENSES_VIEWS = %w[table grid].freeze
  DEFAULT_EXPENSES_VIEW = 'table'
  EXPENSES_VIEWS_ICONS = {
    table: 'table',
    grid: 'apps'
  }.freeze

  hash_branch('expenses') do |r|
    check_csrf!(csrf_failire: :raise)

    r.is do
      r.get do
        result = Expenses::Index.new.call(r.params)
        @filter = result.filter_form
        @scope = result.scope
        @expenses = result.collection.map { |b| Expenses::IndexRowPresenter.new(self, b) }
        @pagination = result.pagination
        @title = model_name(Expense, plural: true)
        @expenses_view_param = r.params['view_as']
        response.set_cookie('bills_view', @expenses_view_param) if @expenses_view_param

        @min_period = Expense.min(:period) || Date.today
        @max_period = Expense.max(:period) || Date.today

        view 'expenses/index'
      end

      r.post do
        @title = t('new.title')
        result = Expenses::Save.new.call(Expense.new, r.params)
        process_result(result,
                       success_redirect: '/expenses',
                       success_flash: 'Expense created',
                       failure_render: 'expenses/new')
      end
    end

    r.get 'new' do
      @title = t('title')
      @form_data = { period_month: Date.today.month }
      view 'expenses/new'
    end

    r.is 'pay' do
      @title = 'Pay bills'
      @form_action = r.path
      r.get do
        @form_data = { paid_at: Date.today }
        view 'expenses/_pay_form'
      end

      r.post do
        result = Expenses::Pay.new.call(r.params)
        process_result(result,
                       success_redirect: '/expenses',
                       success_flash: 'Bills paid',
                       failure_render: 'expenses/_pay_form')
      end
    end

    r.on Integer do |id|
      @expense = Expense[id]
      r.halt(404) unless @expense

      r.is 'edit' do
        @title = "Editing expense №#{@expense.id}"
        r.get do
          @form_data = @expense.form_data
          view 'expenses/edit'
        end

        r.post do
          result = Expenses::Save.new.call(@expense, r.params)
          process_result(result,
                         success_redirect: '/expenses',
                         success_flash: 'Expense updated',
                         failure_render: 'expenses/edit')
        end
      end

      r.is 'delete' do
        @title = "Deleting expense №#{@expense.id}"
        r.get do
          view 'shared/confirm_form',
               locals: { action: r.path, message: "Are you sure you want to delete expense #{@expense.id}?",
                         mode: 'danger' }
        end

        r.post do
          @expense.destroy
          flash[:success] = 'Expense deleted'
          redirect '/expenses'
        end
      end
    end
  end

  def expenses_index_path(base_params = request.params, **extra_params)
    params = base_params.merge(extra_params.transform_keys(&:to_s))
    URI('/expenses')
      .tap { |uri| uri.query = params.to_query if params.present? }
      .to_s
  end

  def expense_edit_path(expense)
    "/expenses/#{PathsHelper.model_or_id(expense)}/edit"
  end

  def expense_class(expense)
    [
      ('is-paid' if expense.paid?),
      ('is-urgent' if expense.urgent?)
    ].compact.first
  end

  def expenses_view
    @expenses_view ||=
      begin
        view = @expenses_view_param || request.cookies['bills_view']
        view.in?(EXPENSES_VIEWS) ? view : DEFAULT_EXPENSES_VIEW
      end
  end
end
