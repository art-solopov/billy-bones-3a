# frozen_string_literal: true

class BillyBones
  hash_branch('categories') do |r|
    r.is do
      r.get do
        categories_index(r)
      end
    end

    r.is 'new' do
      @category = Category.new
      @title = t('title')
      r.get { categories_new(r) }
      r.post { categories_create(r) }
    end

    r.on Integer do |id|
      @category = Category[id]
      r.halt 404 unless @category

      r.is 'edit' do
        @title = t('title', name: @category.name)
        r.get { categories_edit(r) }
        r.post { categories_update(r) }
      end
    end
  end

  def categories_index(_request)
    @title = t('title')
    @categories = Category.roots_default_order_with_children
    view 'categories/index'
  end

  def categories_new(_request)
    @form_data = {}
    view 'categories/new'
  end

  def categories_create(request)
    @category = Category.new
    save_category(request.params, 'new')
  end

  def categories_edit(_request)
    @form_data = { name: @category.name, parent_id: @category.parent_id }
    view 'categories/edit'
  end

  def categories_update(request)
    save_category(request.params, 'edit')
  end

  def root_categories
    @root_categories ||= Category.roots.order(:name)
  end

  def category_parent_options
    root_categories
      .yield_self { |s| @category.new? ? s : s.where { |o| o.id !~ @category.id } }
      .map { |category| category_option(category) }
      .prepend([])
  end

  def save_category(params, action)
    result = ModelSave.new(CategoryContract, %i[name parent_id]).call(@category, params)
    process_result(result, success_redirect: '/categories',
                           success_flash: t('flash'),
                           failure_render: "categories/#{action}")
  end
end
