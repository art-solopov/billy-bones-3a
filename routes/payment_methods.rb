# frozen_string_literal: true

class BillyBones
  hash_branch('payment_methods') do |r|
    check_csrf!(csrf_failire: :raise)

    r.is do
      r.get do
        @title = model_name(PaymentMethod, plural: true)
        @list = PaymentMethod.order(:name)
        view 'payment_methods/index'
      end
    end

    r.is 'new' do
      @title = t('title')
      @payment_method = PaymentMethod.new
      r.get do
        @form_data = {}
        view 'payment_methods/new'
      end

      r.post do
        save_payment_method 'new', t('flash')
      end
    end

    r.on Integer do |id|
      @payment_method = PaymentMethod[id]
      r.halt 404 unless @payment_method

      r.is 'edit' do
        @title = t('title', name: @payment_method.name)
        r.get do
          @form_data = { name: @payment_method.name }
          view 'payment_methods/edit'
        end
        r.post do
          save_payment_method 'edit', t('flash')
        end
      end
    end
  end

  private

  def edit_payment_method_path(id)
    "/payment_methods/#{id}/edit"
  end

  def save_payment_method(action, success_flash, params = request.params)
    result = PaymentMethods::Save.new.call(@payment_method, params)
    process_result(result,
                   success_redirect: '/payment_methods',
                   success_flash: success_flash,
                   failure_render: "payment_methods/#{action}")
  end
end
