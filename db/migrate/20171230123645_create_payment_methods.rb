Sequel.migration do
  change do

    create_table :payment_methods do
      primary_key :id
      String :name, size: 1023, index: true

      DateTime :created_at
      DateTime :updated_at
    end

  end
end
