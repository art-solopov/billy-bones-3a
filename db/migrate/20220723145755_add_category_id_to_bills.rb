# frozen_string_literal: true

Sequel.migration do
  change do
    alter_table :bills do
      add_foreign_key :category_id, :categories
      add_index :category_id
    end
  end
end
