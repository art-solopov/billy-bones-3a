# frozen_string_literal: true

Sequel.migration do
  change do
    alter_table(:categories) do
      add_index Sequel.function(:lower, :name), where: 'parent_id IS NULL', unique: true
      add_index [:parent_id, Sequel.function(:lower, :name)], unique: true
    end
  end
end
