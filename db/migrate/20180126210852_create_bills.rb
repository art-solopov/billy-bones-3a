Sequel.migration do
  change do

    create_table :bills do
      primary_key :id
      foreign_key :payment_method_id, :payment_methods, on_delete: :set_null

      String :state, size: 255, index: true
      Date :period, index: true
      BigDecimal :cost, size: [10, 2]
      Date :paid_at
      column :tags, 'text[]', index: { type: 'GIN' }
    end

  end
end
