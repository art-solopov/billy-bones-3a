Sequel.migration do
  change do
    alter_table :bills do
      add_column :comment, String, text: true
    end
  end
end
