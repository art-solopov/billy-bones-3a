# frozen_string_literal: true

Sequel.migration do
  change do
    create_table :categories do
      primary_key :id
      String :name, size: 1023

      foreign_key :parent_id, :categories, on_delete: :set_null
      index :parent_id
    end
  end
end
