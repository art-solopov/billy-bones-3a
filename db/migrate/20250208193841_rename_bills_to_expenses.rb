# frozen_string_literal: true

Sequel.migration do
  change do
    rename_table :bills, :expenses
  end
end
