# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) { |repo_name| "https://github.com/#{repo_name}" }

gem 'roda'
gem 'puma'
gem 'rack', require: false
gem 'rack-unreloader', require: 'rack/unreloader'
gem 'zeitwerk'

gem 'pg'
gem 'sequel'

gem 'anyway_config'
gem 'enumerize'
gem 'activesupport', require: 'active_support/all'
gem 'dry-validation'
gem 'dry-monads', require: %w[dry-monads dry/monads/do]
gem 'dry-struct'
gem 'jwt'
gem 'faraday'
gem 'faraday-net_http'

gem 'i18n'
gem 'slim'
gem 'forme'
gem 'tubby'

gem 'awesome_print'

gem 'rake'

group :development, :test do
  gem 'pry'
  gem 'pry-byebug'
  gem 'pry-doc'
  gem 'better_errors'
  gem 'binding_of_caller'

  gem 'rubocop'

  gem 'factory_bot'
  gem 'faker'

  gem 'rspec'
  gem 'rack-test'

  gem 'listen'
end

group :test do
  gem 'database_cleaner-sequel'
end
